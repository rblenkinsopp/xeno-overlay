# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit user

DESCRIPTION=""
HOMEPAGE=""
MY_PV=${PV/_alpha*/}
MY_PV=${PV/_beta*/}
MY_PV=${PV/_rc*/}
SRC_URI="http://dl.ubnt.com/unifi/${MY_PV}/UniFi.unix.zip -> ${P}.zip"

LICENSE=""
SLOT="0"
KEYWORDS="amd64"
IUSE=""

RDEPEND="
dev-db/mongodb
>=virtual/jre-1.6"
DEPEND="${RDEPEND}"

DEST="/opt/${PN}"
S="${WORKDIR}/UniFi"

pkg_setup()
{
	enewgroup "unifi"
	enewuser "unifi" -1 -1 ${DEST} "unifi"
}

src_install()
{
	newinitd "${FILESDIR}/${PN}.initd" ${PN}
	dodoc "readme.txt"
	insinto ${DEST}
	doins -r "."
	fowners -R unifi:unifi ${DEST}
}

pkg_preinst()
{
	# Remove previous webapps to prevent errors.
	rm -rf "${DEST}/webapps"
}
